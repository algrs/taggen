from django.db import models

class BaseTag(object):
    @property
    def header(self):
        return "{}: {}".format(self.short_description, self.title)
    
    @property
    def header_html(self):
        return "<b>{}</b>: {}".format(self.short_description, self.title)
    
    def _body(self, use_markdown=False):
        import markdown
        if use_markdown:
            result = [markdown.markdown(self.text)]
        else:
            result = [self.text]
        if hasattr(self, 'footer') and len(self.footer) > 0:
            result.append(self.footer)
        if hasattr(self, 'uses') and self.uses > 1:
            result.append("O" * self.uses)
        if hasattr(self, 'expires') and self.expires:
            result.append("Expires in {} months".format(self.shelf_life))
        return result

    @property
    def body(self):
        return "\n\n".join(self._body())

    @property
    def body_html(self):
        return "\n".join(["<p>{}</p>".format(x) for x in self._body(use_markdown=True)])

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.__str__()

    def __repr__(self):
        return self.__str__()


class RationTag(BaseTag, models.Model):
    short_description = 'Ration'
    title = models.CharField(max_length=50)
    text = models.TextField(blank=True)
    expires = True
    shelf_life = models.IntegerField(default=12)
    paper = "green"
    size = "card"
    footer = "This is a ration and good for eatin'"

class ItemTag(BaseTag, models.Model):
    short_description = 'Item'
    title = models.CharField(max_length=50)
    text = models.TextField(blank=True)
    expires = models.BooleanField(default=False)
    shelf_life = models.IntegerField(default=12)
    uses = models.IntegerField(default=1)
    paper = "blue"
    size = "card"

class ComponentColor(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.name

class ComponentCategory(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = "component categories"

class ComponentTag(BaseTag, models.Model):
    short_description = 'Component'
    title = models.CharField(max_length=50)
    text = models.TextField(blank=True)
    color = models.ForeignKey(ComponentColor)
    category = models.ForeignKey(ComponentCategory)
    expires = False
    paper = "blue"
    size = models.CharField(max_length=20, choices=[
        ("card", "card"),
        ("half card", "half card"),
    ], default='card')
    
    @property
    def footer(self):
        return "{} {}".format(self.color, self.category)
    
class EffectTag(BaseTag, models.Model):
    short_description = 'Effect'
    title = models.CharField(max_length=50)
    text = models.TextField(blank=True)
    paper = "yellow"
    size = models.CharField(max_length=20, choices=[
        ("card", "card"),
        ("quarter page", "quarter page"),
    ], default='card')
