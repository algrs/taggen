from django import forms
from django.forms.formsets import formset_factory

from .models import *

class ReallyItsALabelWidget(forms.widgets.Widget):
    def render(self, name, value, attrs=None):
        return '<input id="{}" name="{}" type="hidden" value="{}">{}'.format(name, name, value, value)

class TagQuantityForm(forms.Form):
    tag = forms.CharField(widget=ReallyItsALabelWidget())
    tag_id = forms.IntegerField(widget=forms.HiddenInput())
    count = forms.IntegerField(initial=1, min_value=1)

TagQuantityFormSet = formset_factory(TagQuantityForm, extra=0)
