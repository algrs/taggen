import logging
from django.contrib import admin
from django.shortcuts import render
from django.contrib.admin import helpers

from .models import *
from .forms import TagQuantityFormSet

def expand_tags(queryset, counts):
    logger = logging.getLogger("xovertags")
    logger.warning("expand_tags {} {}".format(queryset, counts))

    def tag_for_pk(pk):
        return [x for x in queryset if x.pk == pk][0]
    objects = []
    for x in counts:
        objects.extend([tag_for_pk(x['tag_id'])] * x['count'])
    return objects

def print_tags(modeladmin, request, queryset):
    logger = logging.getLogger("xovertags")
    logger.warning("print_tags {} {}".format(request.POST, queryset))
    if 'do_action' in request.POST:
        formset = TagQuantityFormSet(request.POST)
        if formset.is_valid():
            return render(request, 'admin/tags.html', {
                'tags': expand_tags(queryset, formset.cleaned_data),
            })
    else:
        formset = TagQuantityFormSet(initial=[
            {
                'tag_id': tag.pk,
                'tag': tag.title,
            }
            for tag in queryset.all()
        ])

    return render(request, 'admin/action_print.html', {
        'title': u'Choose how many to print',
        'objects': queryset,
        'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
        'form': formset,
    })
print_tags.short_description = "Print Tags"

def has_custom_text(tag):
    return len(tag.text) > 0
has_custom_text.short_description = 'Has Custom Text'
has_custom_text.boolean = True

class RationTagAdmin(admin.ModelAdmin):
    list_display = ['title', 'shelf_life', has_custom_text]
    list_filter = ['title']
    search_fields = ['title']
    fieldsets = (
        (None, {
            'fields': (('short_description', 'title'), )
        }),
        (None, {
            'fields': ('text', 'footer')
        }),
        (None, {
            'fields': ('shelf_life', ('paper', 'size'))
        }),
    )
    readonly_fields = ['short_description', 'paper', 'size', 'footer']
    save_as = True
    actions = [print_tags]
admin.site.register(RationTag, RationTagAdmin)

class ItemTagAdmin(admin.ModelAdmin):
    list_display = ['title', 'expires', 'shelf_life', 'uses']
    list_filter = ['title']
    search_fields = ['title']
    fieldsets = (
        (None, {
            'fields': (('short_description', 'title'), )
        }),
        (None, {
            'fields': ('text',)
        }),
        (None, {
            'fields': (('expires', 'shelf_life'), 'uses', ('paper', 'size'))
        }),
    )
    readonly_fields = ['short_description', 'paper', 'size']
    save_as = True
    actions = [print_tags]
admin.site.register(ItemTag, ItemTagAdmin)

class ComponentTagAdmin(admin.ModelAdmin):
    list_display = ['title', 'size']
    list_filter = ['title']
    search_fields = ['title']
    fieldsets = (
        (None, {
            'fields': (('short_description', 'title'), )
        }),
        (None, {
            'fields': ('text',)
        }),
        (None, {
            'fields': (('color', 'category'), )
        }),
        (None, {
            'fields': (('paper', 'size'), )
        }),
    )
    radio_fields = {'size': admin.HORIZONTAL}
    readonly_fields = ['short_description', 'paper']
    save_as = True
    actions = [print_tags]
admin.site.register(ComponentTag, ComponentTagAdmin)

class ComponentColorAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    search_fields = ['name']
    fields = ['name']
    save_as = True
admin.site.register(ComponentColor, ComponentColorAdmin)

class ComponentCategoryAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    search_fields = ['name']
    fields = ['name']
    save_as = True
admin.site.register(ComponentCategory, ComponentCategoryAdmin)

class EffectTagAdmin(admin.ModelAdmin):
    list_display = ['title']
    list_filter = ['title']
    search_fields = ['title']
    fieldsets = (
        (None, {
            'fields': (('short_description', 'title'), )
        }),
        (None, {
            'fields': ('text',)
        }),
        (None, {
            'fields': (('paper', 'size'), )
        }),
    )
    readonly_fields = ['short_description', 'paper']
    save_as = True
    actions = [print_tags]
admin.site.register(EffectTag, EffectTagAdmin)
