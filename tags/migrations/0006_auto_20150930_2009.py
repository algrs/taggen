# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0005_auto_20150930_2000'),
    ]

    operations = [
        migrations.CreateModel(
            name='MasterTagSegments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('expires', models.BooleanField(default=False)),
                ('shelf_life', models.IntegerField(default=12)),
                ('uses', models.IntegerField(default=1)),
            ],
        ),
        migrations.RenameField(
            model_name='mastertag',
            old_name='text',
            new_name='tag_text',
        ),
        migrations.RemoveField(
            model_name='mastertag',
            name='uses',
        ),
        migrations.AlterField(
            model_name='componenttag',
            name='size',
            field=models.CharField(default=b'card', max_length=20, choices=[(b'card', b'card'), (b'half card', b'half card')]),
        ),
        migrations.AlterField(
            model_name='mastertag',
            name='size',
            field=models.CharField(default=b'card', max_length=20, choices=[(b'card', b'card'), (b'quarter page', b'quarter page')]),
        ),
        migrations.AddField(
            model_name='mastertagsegments',
            name='master',
            field=models.ForeignKey(to='tags.MasterTag'),
        ),
    ]
