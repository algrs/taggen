# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tags.models


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0004_componenttag'),
    ]

    operations = [
        migrations.CreateModel(
            name='MasterTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('text', models.TextField(blank=True)),
                ('expires', models.BooleanField(default=False)),
                ('shelf_life', models.IntegerField(default=12)),
                ('uses', models.IntegerField(default=1)),
                ('size', models.CharField(default=b'card', max_length=10, choices=[(b'card', b'card'), (b'quarter page', b'quarter page')])),
            ],
            bases=(tags.models.BaseTag, models.Model),
        ),
        migrations.AlterField(
            model_name='componenttag',
            name='size',
            field=models.CharField(default=b'card', max_length=10, choices=[(b'card', b'card'), (b'half card', b'half card')]),
        ),
    ]
