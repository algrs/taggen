# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0002_itemtag'),
    ]

    operations = [
        migrations.AddField(
            model_name='itemtag',
            name='expires',
            field=models.BooleanField(default=False),
        ),
    ]
