# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0009_auto_20151012_1014'),
    ]

    operations = [
        migrations.AddField(
            model_name='componenttag',
            name='category',
            field=models.ForeignKey(default=1, to='tags.ComponentCategory'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='componenttag',
            name='color',
            field=models.ForeignKey(default=1, to='tags.ComponentColor'),
            preserve_default=False,
        ),
    ]
