# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tags.models


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0003_itemtag_expires'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComponentTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('size', models.CharField(max_length=10, choices=[(b'card', b'card'), (b'half card', b'half card')])),
            ],
            bases=(tags.models.BaseTag, models.Model),
        ),
    ]
