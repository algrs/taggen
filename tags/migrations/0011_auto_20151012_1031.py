# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0010_auto_20151012_1021'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mastertagsegments',
            name='master',
        ),
        migrations.AlterModelOptions(
            name='componentcategory',
            options={'verbose_name_plural': 'component categories'},
        ),
        migrations.DeleteModel(
            name='MasterTag',
        ),
        migrations.DeleteModel(
            name='MasterTagSegments',
        ),
    ]
