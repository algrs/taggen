# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0006_auto_20150930_2009'),
    ]

    operations = [
        migrations.AddField(
            model_name='componenttag',
            name='text',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='mastertagsegments',
            name='master',
            field=models.ForeignKey(related_name='segments', to='tags.MasterTag'),
        ),
    ]
