# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tags.models


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0011_auto_20151012_1031'),
    ]

    operations = [
        migrations.CreateModel(
            name='EffectTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('text', models.TextField(blank=True)),
                ('size', models.CharField(default=b'card', max_length=20, choices=[(b'card', b'card'), (b'half card', b'half card')])),
            ],
            bases=(tags.models.BaseTag, models.Model),
        ),
    ]
