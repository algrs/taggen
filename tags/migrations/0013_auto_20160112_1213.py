# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0012_effecttag'),
    ]

    operations = [
        migrations.AlterField(
            model_name='effecttag',
            name='size',
            field=models.CharField(default=b'card', max_length=20, choices=[(b'card', b'card'), (b'quarter page', b'quarter page')]),
        ),
    ]
