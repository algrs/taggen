# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def create_categories(apps, schema_editor):
    ComponentCategory = apps.get_model("tags", "ComponentCategory")
    for name in ['Plant', 'Metal', 'Gem', 'Vapour']:
        new_entry = ComponentCategory(name=name)
        new_entry.save()

def create_colors(apps, schema_editor):
    ComponentColor = apps.get_model("tags", "ComponentColor")
    for name in ['Red', 'Blue', 'Green', 'White', 'Black']:
        new_entry = ComponentColor(name=name)
        new_entry.save()

class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0008_componentcategory_componentcolor'),
    ]

    operations = [
        migrations.RunPython(create_categories),
        migrations.RunPython(create_colors),
    ]
