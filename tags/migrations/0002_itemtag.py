# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tags.models


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ItemTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('text', models.TextField(blank=True)),
                ('shelf_life', models.IntegerField(default=12)),
                ('uses', models.IntegerField(default=1)),
            ],
            bases=(tags.models.BaseTag, models.Model),
        ),
    ]
