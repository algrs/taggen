Tag generator for LARPs

You will need to edit the formatting in `tags/templates/admin/tags.html` to add your game's name, custom fonts, and other details.

You might need to edit the tag types in `tags/models.py`.

* heroku local
* pip freeze --local > requirements.txt
* heroku run python manage.py migrate
* heroku run python manage.py shell

* python manage.py makemigrations
* python manage.py migrate

## License ##

Creative Commons [Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/)